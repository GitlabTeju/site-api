<?php

namespace Drupal\site_api\Plugin\rest\resource;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "site_resource",
 *   label = @Translation("Site API resource"),
 *   uri_paths = {
 *     "canonical" = "/page_json/{key}/{id}",
 *   }
 * )
 */
class SiteApiResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var AccountProxyInterface
   */
  protected $currentUser;

  protected $entity_type_manager;
  
  public $config;

  /**
   * Constructs a new SiteApiResource object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $current_user, ConfigFactory $config, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->config = $config;
	  $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('site_api'),
      $container->get('current_user'),
      $container->get('config.factory'),
	    $container->get('entity.manager')
    );
  }

  /**
   * Responds to GET requests.
   */
  public function get($key, $id) {
    $systemConfig = $this->config->get('system.site');
    $siteapikey = $systemConfig->get('siteapikey');
    //use to load the node data
      $entity = $this->entityTypeManager->getStorage('node')->load($id);
    $type = '';
    
    //use to check if node exist.
    if(!empty($entity)) {
      $type = $entity->getType();
    }

    // Use current user after pass authentication to validate access and check correct apikey and node type.
    if (!$this->currentUser->hasPermission('access content') || $key != $siteapikey || $type != 'page') {
	  $response = ['message' => 'access denied'];
      return new ResourceResponse($response);
    }
    
    return new ResourceResponse($entity, 200);
  }

}
