## Site API module

Provides the json response of a node based on site api key provided in site information config..
    : You can change the API key at basic system site configuration.
    : Exposing the basic page content in json format using rest api plugin at URL http://localhost/page_json/FOOBAR12345/17  where 'FOOBAR12345' API key and '17' node ID.
